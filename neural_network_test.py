import os
from PIL import Image as Image
import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
import cv2
from tqdm import tqdm

bins = 8
image_x_size = 100
image_y_size = 100


#reads and downsizes images from a folder and returns it as an NP array 
def imagesFromFolder(fname):
    image_list = []
    list_files = os.listdir(fname)
    for filename in list_files:
        filename = os.path.join(fname, filename)
        image_list.append(filename)
    print('reading Images')
    for i in tqdm(range(0,len(image_list))):
        image_list[i] = Image.open(image_list[i])
        image_list[i].thumbnail((image_x_size,image_y_size))
        image_list[i]= np.array(image_list[i]) /255
    return np.array(image_list)

emptyImage = imagesFromFolder('under_no_touch')
snusImage = imagesFromFolder('under_touch')
emptyX = [a.ravel() for a in emptyImage]
snusX = [a.ravel() for a in snusImage]
X = np.concatenate((emptyX,snusX))
Y= [0]*len(emptyImage) + [1]*len(snusImage)
Y = np.array(Y)
X_train,X_test,y_train,y_test = train_test_split(
    X,
    Y,
    test_size = 0.2,
    shuffle = True,
    random_state = 42
)

clf = MLPClassifier(verbose=1,hidden_layer_sizes=200,max_iter=500,n_iter_no_change=200)
clf.fit(X,Y)

predicted = clf.predict(X_test)
success = 0
fail = 0
for i in range(len(predicted)):
    if(y_test[i] == predicted[i]):
        success += 1
    else:
        fail+=1

print(success)
print(fail)
#save model
from joblib import dump, load

dump(clf, 'under.joblib')
