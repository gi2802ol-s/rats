
import sys
from PIL import Image as Image
import numpy as np
from sklearn.model_selection import train_test_split
import cv2
import os
from tqdm import tqdm


bins = 8
image_x_size = 100
image_y_size = 100

def imagesFromFolder(fname):
    image_list = []
    list_files = os.listdir(fname)
    for filename in list_files:
        filename = os.path.join(fname, filename)
        image_list.append(filename)
    print('reading Images')
    for i in tqdm(range(0,len(image_list))):
        image_list[i] = Image.open(image_list[i])
        image_list[i].thumbnail((image_x_size,image_y_size))
        image_list[i]= np.array(image_list[i]) /255
        image_list[i]= image_list[i].ravel()
    return np.array(image_list)

#reads and downsizes image
def imageProcess(fname):
    im = Image.open(fname)
    im.thumbnail((image_x_size,image_y_size))
    im= np.array(im).ravel().reshape(1,-1)
    return im


from joblib import dump, load

clf = load('under.joblib')

predict = clf.predict(imagesFromFolder(sys.argv[1]))

print((sum(predict)/len(predict)))