import sys
import os
import re
from numpy.core.fromnumeric import argmax
from tqdm import tqdm
import cv2
from PIL import Image as Image
import numpy as np
import shutil


#Filters the predicted classes from the trained input model into seperate subfolders for each class. Arguments: {movie} {trained-model}, ex: "#python3 predict_from_movie.py cropped.mkv under.joblib"

root_dir = os.getcwd()
dirs = list(filter(lambda name: bool(re.search('classified_frames_', name)),os.listdir()))
dir = 'classified_frames_'+str(len(dirs))
fpath = sys.argv[1]
os.mkdir(dir)

cap= cv2.VideoCapture(fpath)
os.chdir(os.path.join(os.getcwd(),dir))

print("Extracting frames from,", fpath,"to", dir)
for i in tqdm(range(int(cap.get(cv2.CAP_PROP_FRAME_COUNT)))):
    ret, frame = cap.read()
    cv2.imwrite('frame'+str(i)+'.jpg',frame)
    i+=1
print("Done")
cap.release()
cv2.destroyAllWindows()
os.chdir(root_dir)

bins = 8
image_x_size = 100
image_y_size = 100

def imagesFromFolder(fname):
    image_list = []
    list_files = os.listdir(fname)
    for filename in list_files:
        filename = os.path.join(fname, filename)
        image_list.append(filename)
    print('reading Images')
    for i in tqdm(range(0,len(image_list))):
        image_list[i] = Image.open(image_list[i])
        image_list[i].thumbnail((image_x_size,image_y_size))
        image_list[i]= np.array(image_list[i]) /255
        image_list[i]= image_list[i].ravel()
    return np.array(image_list)

#reads and downsizes image
def imageProcess(fname):
    im = Image.open(fname)
    im.thumbnail((image_x_size,image_y_size))
    im= np.array(im).ravel().reshape(1,-1)
    return im

#Filters frames into class folders depending on predicted class
def filterFolder(fname, filterList):
    image_list = []
    list_files = os.listdir(fname)
    for filename in list_files:
        image_list.append(filename)
    nbr_of_classes = filterList[argmax(filterList)]
    os.chdir(os.path.join(os.getcwd(),fname))
    print('Filtering into class folders')
    for c in range(nbr_of_classes+1):    
        os.mkdir('class_'+str(c))
        for i in range(len(filterList)):
            if filterList[i] == c:
                shutil.move(os.path.join(os.getcwd(), image_list[i]), os.path.join('class_'+str(c), image_list[i]))

from joblib import dump, load

model = sys.argv[2]
clf = load(str(model))

predict = clf.predict(imagesFromFolder(dir))

filterFolder(dir,predict)
