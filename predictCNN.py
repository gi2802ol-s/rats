
import numpy as np
import tensorflow as tf
from keras.models import load_model
from keras.metrics import categorical_crossentropy
from keras.preprocessing.image import ImageDataGenerator
import os
import matplotlib.pyplot as plt
import sys
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

batch_size = 1

def number_of_data_folder(fname):
    totalFiles = 0
    for base, dirs, files in os.walk(fname):
        for Files in files:
            totalFiles += 1
    return totalFiles

model_name = sys.argv[1] + ".h4"

predict_path = sys.argv[2]
train_N = number_of_data_folder(predict_path)

predict_batches = ImageDataGenerator(preprocessing_function=tf.keras.applications.vgg16.preprocess_input)\
    .flow_from_directory(directory=predict_path, target_size = (224,224), batch_size=batch_size, class_mode=None)

model = load_model(model_name)

predictions = model.predict(predict_batches, steps = train_N/batch_size ,verbose =1)
predictions01 = [list(a).index(max(a)) for a in predictions]
print(predictions)
print(predictions01)