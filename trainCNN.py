
import numpy as np
import tensorflow as tf
from keras.models import Sequential, load_model
from keras.layers import Dense, Activation,Flatten, BatchNormalization, Conv2D, MaxPool2D, Dropout
from tensorflow import keras
from keras.metrics import categorical_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from kerastuner import RandomSearch
from kerastuner.engine.hyperparameters import HyperParameters
import os
import matplotlib.pyplot as plt
import sys
import warnings
import sklearn
import math
import datetime

warnings.simplefilter(action='ignore', category=FutureWarning)
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

batch_size = 50
epochs = 25

def labels_from_folder(fname):
    list_files = os.listdir(fname)
    return list_files

def number_of_data_folder(fname):
    totalFiles = 0
    for base, dirs, files in os.walk(fname):
        for Files in files:
            totalFiles += 1
    return totalFiles

def create_class_weight(labels_dict,mu=0.15):
    total = np.sum(list(labels_dict.values()))
    keys = labels_dict.keys()
    class_weight = dict()

    for key in keys:
        score = math.log(mu*total/float(labels_dict[key]))
        class_weight[key] = score if score > 1.0 else 1.0

    return class_weight

"""def build_model(hp):
    model = keras.Sequential([
        keras.layers.Conv2D(
            filters = hp.Int('conv_1_filter', min_value=16, max_value=64, step=8),
            kernel_size = hp.Choice('conv_1_kernel', values = [3,4,5]),
            activation = 'relu',
            input_shape = (56,56,3)
        ),
        keras.layers.Conv2D(
            filters = hp.Int('conv_2_filter', min_value=16, max_value=64, step=8),
            kernel_size = hp.Choice('conv_2_kernel', values = [3,4,5]),
            activation = 'relu'
        ),
        keras.layers.Conv2D(
            filters = hp.Int('conv_3_filter', min_value=16, max_value=64, step=8),
            kernel_size = hp.Choice('conv_3_kernel', values = [3,4,5]),
            activation = 'relu'
        ),
        keras.layers.MaxPool2D(pool_size=(2,2), strides=2),
        keras.layers.Conv2D(
            filters = hp.Int('conv_4_filter', min_value=32, max_value=128, step=16),
            kernel_size = hp.Choice('conv_4_kernel', values = [3,4,5]),
            activation = 'relu',
            input_shape = (56,56,3)
        ),
        keras.layers.Conv2D(
            filters = hp.Int('conv_5_filter', min_value=32, max_value=128, step=16),
            kernel_size = hp.Choice('conv_5_kernel', values = [3,4,5]),
            activation = 'relu'
        ),
        keras.layers.Conv2D(
            filters = hp.Int('conv_6_filter', min_value=32, max_value=128, step=16),
            kernel_size = hp.Choice('conv_6_kernel', values = [3,4,5]),
            activation = 'relu'
        ),
        keras.layers.MaxPool2D(pool_size=(2,2), strides=2),
        keras.layers.Flatten(),
        keras.layers.Dense(
            units = hp.Int('dense_1_unites', min_value=32, max_value=128, step=16),
            activation = 'relu'
        ),
        keras.layers.Dense(
            units = hp.Int('dense_2_unites', min_value=16, max_value=64, step=8),
            activation = 'relu'
        ),
        keras.layers.Dense(output_N, activation='softmax')
    ])

    model.compile(optimizer=keras.optimizers.Adam(hp.Choice('learning_rate', values=[1e-2, 1e-3])),
                loss='categorical_crossentropy',
                metrics=['accuracy'])

    return model"""

#dict for label weights
#labels_dict = {0: 2550, 1: 1143, 2: 7188, 3: 78, 4: 600}
#class_weights = create_class_weight(labels_dict)
#print(class_weights)

model_name = sys.argv[1] + ".h4"

train_path = sys.argv[2]
train_labels = labels_from_folder(train_path)
train_N = number_of_data_folder(train_path)

valid_path = sys.argv[3]
valid_labels = labels_from_folder(valid_path)
valid_N = number_of_data_folder(valid_path)

output_N = len(train_labels)

print(train_path, train_labels)
print(valid_path, valid_labels)

train_batches = ImageDataGenerator(rotation_range=360,channel_shift_range=30,rescale=1/255,preprocessing_function=tf.keras.applications.vgg16.preprocess_input)\
    .flow_from_directory(directory=train_path, target_size = (112,112), classes=train_labels,batch_size=batch_size)

validation_batches = ImageDataGenerator(rescale=1/255,preprocessing_function=tf.keras.applications.vgg16.preprocess_input)\
    .flow_from_directory(directory=valid_path, target_size = (112,112), classes=valid_labels, batch_size=batch_size)

imgs, labels = next(train_batches)

#tuner_search = RandomSearch(build_model, objective='val_accuracy', max_trials=5, directory='output', project_name=model_name)

"""tuner_search.search(
    x=train_batches,
    validation_data=validation_batches,
    epochs=3,
    batch_size=batch_size,
    steps_per_epoch=train_N/batch_size,
    validation_steps=valid_N/batch_size)"""

model = Sequential([
    Conv2D(filters=32, kernel_size=(3,3), activation='relu', padding='same', input_shape=(112,112,3)),
    Conv2D(filters=32, kernel_size=(3,3), activation='relu', padding='same'),
    Conv2D(filters=32, kernel_size=(3,3), activation='relu', padding='same'),
    MaxPool2D(pool_size=(2,2), strides=2),
    Conv2D(filters=64, kernel_size=(3,3), activation='relu', padding='same'),
    Conv2D(filters=64, kernel_size=(3,3), activation='relu', padding='same'),
    Conv2D(filters=64, kernel_size=(3,3), activation='relu', padding='same'),
    MaxPool2D(pool_size=(2,2), strides=2),
    Flatten(),
    Dense(units=128,activation='relu'),
    Dense(units=64,activation='relu'),
    Dense(units=output_N, activation="softmax")
])

model.compile(metrics=['accuracy'],loss='categorical_crossentropy')

log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

model.fit(
    x=train_batches,
    validation_data=validation_batches,
    batch_size=batch_size,
    epochs=epochs,
    shuffle=True,
    verbose=1,
    steps_per_epoch=train_N/batch_size,
    validation_steps=valid_N/batch_size,
    callbacks=[tensorboard_callback])

model.save(model_name)
