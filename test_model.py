#Usage: python3 test_model *model_name* *tagged_folder0* ... *tagged_folderN*
#Uses *model_name* to predict tags of the input tagged folders and prints the accuracy
# IMPORTANT: It's important to write the tagged folders in the same order as when training the model
import sys
from PIL import Image as Image
import numpy as np
import os
from tqdm import tqdm


bins = 8
image_x_size = 100
image_y_size = 100

#reads and downsizes frames from a folder and returns it as an NP array 
def imagesFromFolder(fname):
    image_list = []
    list_files = os.listdir(fname)
    for filename in list_files:
        filename = os.path.join(fname, filename)
        image_list.append(filename)
    print('reading images from folder', fname)
    for i in tqdm(range(0,len(image_list))):
        image_list[i] = Image.open(image_list[i])
        image_list[i].thumbnail((image_x_size,image_y_size))
        image_list[i]= np.array(image_list[i]) /255
        image_list[i]= image_list[i].ravel()
    return np.array(image_list)


from joblib import dump, load

clf = load(sys.argv[1]+'.joblib')
print("Testing model", sys.argv[1])
results = []
for i in range(len(sys.argv))[2:]:
    predict = clf.predict(imagesFromFolder(sys.argv[i]))
    correct = 0
    for prediction in predict:
        if prediction == i-2:
            correct += 1
    ratio = correct/len(predict)
    results.append(["Model predicted frames from folder", sys.argv[i], "and got an accuracy of",ratio])
for result in results:
    print(*result)