#Usage: python3 train_model *model_name* *tagged_folder0* ... *tagged_folderN*
#Outputs a saved model with name *model_name*

from PIL import Image as Image
import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from tqdm import tqdm
import sys
import os


#settings for downsampling frames
bins = 8
image_x_size = 100
image_y_size = 100


#reads and downsizes frames from a folder and returns it as an NP array 
def imagesFromFolder(fname):
    image_list = []
    list_files = os.listdir(fname)
    for filename in list_files:
        filename = os.path.join(fname, filename)
        image_list.append(filename)
    print('reading images from folder', fname)
    for i in tqdm(range(0,len(image_list))):
        image_list[i] = Image.open(image_list[i])
        image_list[i].thumbnail((image_x_size,image_y_size))
        image_list[i]= np.array(image_list[i]) /255
    return np.array(image_list)

#reads all tagged folders and creates X and y vectors for training
data = []
for folder in sys.argv[2:]:
   data.append([a.ravel() for a in imagesFromFolder(folder)])
X = np.concatenate(data)
y = []
for i in range(len(data)):
    y = y+[i] * len(data[i])
X = np.array(X)
y = np.array(y)

#splits X and y vectors to training and test sets
X_train,X_test,y_train,y_test = train_test_split(
    X,
    y,
    test_size = 0.2,
    shuffle = True,
    random_state = 42
)
#trains model
print("Training model")
clf = MLPClassifier(verbose=1,hidden_layer_sizes=200,max_iter=500,n_iter_no_change=200)
clf.fit(X_train,y_train)

#tests trained model
predicted = clf.predict(X_test)
success = 0
fail = 0
for i in range(len(predicted)):
    if(y_test[i] == predicted[i]):
        success += 1
    else:
        fail+=1
print("successfully identified", success, "frames, and missed",fail,"frames from test set")
#save model
from joblib import dump, load

dump(clf, sys.argv[1]+'.joblib')
print("saved model as",sys.argv[1]+'.joblib')
