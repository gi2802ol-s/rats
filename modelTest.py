
import sys
from PIL import Image as Image
import numpy as np
from sklearn.model_selection import train_test_split
import cv2


bins = 8
image_x_size = 20
image_y_size = 20


#reads and downsizes image
def imageProcess(fname):
    im = Image.open(fname)
    im.thumbnail((image_x_size,image_y_size))
    im= np.array(im)
    return im

#extracts color histogram from image
def fd_histogram(image,mask =None):
    # convert the image to HSV color-space
    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    # compute the color histogram
    hist  = cv2.calcHist([image], [0, 1, 2], None, [bins, bins, bins], [0, 256, 0, 256, 0, 256])
    # normalize the histogram
    cv2.normalize(hist, hist)
    # return the histogram
    return hist.flatten()


from joblib import dump, load

clf = load('test.joblib')

print(clf.predict(fd_histogram(imageProcess(sys.argv[1])).reshape(1,-1)))