import math

#seperates list into lists of left and right touches
def seperate(hand_list):
    left = []
    right = []
    for i in hand_list:
        if i == 0:
            left.append(0)
            right.append(0)
        if i == 1:
            left.append(1)
            right.append(0)
        if i == 2:
            left.append(0)
            right.append(1)
        if i == 3:
            left.append(1)
            right.append(1)
    return left,right

#smoothes the list using a moving average of window size N

def smooth(hand_list, N):
    smoothed = []
    for i in range(len(hand_list)):
        if sum(hand_list[i: i+N]) >= N/2:
            smoothed.append(1)
        else:
            smoothed.append(0)
    return smoothed

#condenses list, i.e
# [1,1,1,1,0,0,0,1,1,1,1] becomes [1,0,1]
def condense(hand_list):
    mem = -1
    output = []
    for i in hand_list:
        if i != mem:
            output.append(i)
            mem = i
    return output

#applies all the functiouns above to count left and right touches in a vector
def count_touches(hand_list,smoothing_factor):
    left,right = seperate(hand_list)
    left_count = sum(condense(smooth(left,smoothing_factor)))
    right_count = sum(condense(smooth(right,smoothing_factor)))
    return left_count,right_count


#just to test everything
test = [1,1,1,1,1,1,0,0,1,1,0,1,1,1,2,2,2,0,0,0,1,1,3,3,3,3,3,2,2,2,0,0,0,0,0,1,0,0]

print(count_touches(test,2))
