import sys
import os
import re
from tqdm import tqdm
import cv2
import numpy as np
import time

dirs = list(filter(lambda name: bool(re.search('extracted_frames', name)),os.listdir()))
dir = 'extracted_frames_'+str(len(dirs))
fpath = sys.argv[1]
os.mkdir(dir)

cap= cv2.VideoCapture(fpath)
os.chdir(os.path.join(os.getcwd(),dir))
os.mkdir("data")
os.chdir(os.path.join(os.getcwd(),"data"))
print("Extracting frames from,", fpath,"to", dir)
for i in tqdm(range(int(cap.get(cv2.CAP_PROP_FRAME_COUNT)))):

    ret, frame = cap.read()

    if i == 0:
        img = cv2.medianBlur(frame,5)
        img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

        circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1,20,
                                    param1=50,param2=100,minRadius=100,maxRadius=470)
        j = np.uint16(np.around(circles))[0][0]
        print(j)
        height,width = img.shape
        mask = np.zeros((height,width), np.uint8)
        cv2.circle(img,(j[0],j[1]),j[2],(0,255,0),2)
        # draw the center of the circle
        cv2.circle(img,(j[0],j[1]),2,(0,0,255),3)
        circle_img = cv2.circle(mask,(j[0],j[1]),j[2],(255,255,255),thickness=-1)

    masked_data = cv2.bitwise_and(frame, frame, mask=circle_img)
    _,thresh = cv2.threshold(mask,1,255,cv2.THRESH_BINARY)
    contours = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    x,y,w,h = cv2.boundingRect(contours[0][0])
    crop = masked_data[y:y+h,x:x+w]
    cv2.imwrite('frame'+str(i)+'.jpg',crop)
    i+=1

print("Done")
cap.release()
cv2.destroyAllWindows()
